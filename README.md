# Music Max BE

Backend for Music Max application project

Inside src/main directory create subdirectory named 'resources' and under that folder create file named 'application.properties'.  
  
In application.properties file put these lines:  

spring.datasource.url=jdbc:mysql://localhost:3306/dbname  
spring.datasource.username=dbusername  
spring.datasource.password=dbpassword  
spring.datasource.driver-class-name=com.mysql.jdbc.Driver  
server.port=8080  
spring.jpa.hibernate.naming.physical-strategy=org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl  
spring.jpa.properties.hibernate.id.new_generator_mappings=false  
spring.jpa.show-sql=true  
spring.servlet.multipart.max-file-size=10MB  
spring.servlet.multipart.max-request-size=10MB  
