-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema db_musicmax
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema db_musicmax
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `db_musicmax` DEFAULT CHARACTER SET utf8 ;
USE `db_musicmax` ;

-- -----------------------------------------------------
-- Table `db_musicmax`.`izvodjac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`izvodjac` (
  `idizvodjac` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `prezime` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idizvodjac`),
  UNIQUE INDEX `idizvodjac_UNIQUE` (`idizvodjac` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`korisnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`korisnik` (
  `idkorisnik` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `prezime` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `sifra` VARCHAR(45) NOT NULL,
  `brojpesama` INT NOT NULL,
  PRIMARY KEY (`idkorisnik`),
  UNIQUE INDEX `idkorisnik_UNIQUE` (`idkorisnik` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`autor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`autor` (
  `idautor` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `prezime` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idautor`),
  UNIQUE INDEX `idautor_UNIQUE` (`idautor` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`pravac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`pravac` (
  `idpravac` INT NOT NULL AUTO_INCREMENT,
  `naziv` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idpravac`),
  UNIQUE INDEX `idpravac_UNIQUE` (`idpravac` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`pesma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`pesma` (
  `idpesma` INT NOT NULL AUTO_INCREMENT,
  `naziv` VARCHAR(45) NOT NULL,
  `vremenastanka` YEAR(4) NOT NULL,
  `izvodjac_idizvodjac` INT NOT NULL,
  `korisnik_idkorisnik` INT NOT NULL,
  `audiozapis` BLOB NULL,
  `videozapis` BLOB NULL,
  `autor_idautor` INT NOT NULL,
  `pravac_idpravac` INT NOT NULL,
  PRIMARY KEY (`idpesma`),
  UNIQUE INDEX `idpesma_UNIQUE` (`idpesma` ASC) ,
  INDEX `fk_pesma_izvodjac_idx` (`izvodjac_idizvodjac` ASC) ,
  INDEX `fk_pesma_korisnik1_idx` (`korisnik_idkorisnik` ASC) ,
  INDEX `fk_pesma_autor1_idx` (`autor_idautor` ASC) ,
  INDEX `fk_pesma_pravac1_idx` (`pravac_idpravac` ASC) ,
  CONSTRAINT `fk_pesma_izvodjac`
    FOREIGN KEY (`izvodjac_idizvodjac`)
    REFERENCES `db_musicmax`.`izvodjac` (`idizvodjac`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pesma_korisnik1`
    FOREIGN KEY (`korisnik_idkorisnik`)
    REFERENCES `db_musicmax`.`korisnik` (`idkorisnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pesma_autor1`
    FOREIGN KEY (`autor_idautor`)
    REFERENCES `db_musicmax`.`autor` (`idautor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pesma_pravac1`
    FOREIGN KEY (`pravac_idpravac`)
    REFERENCES `db_musicmax`.`pravac` (`idpravac`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`zanimljivosti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`zanimljivosti` (
  `idzanimljivosti` INT NOT NULL AUTO_INCREMENT,
  `naslov` VARCHAR(45) NOT NULL,
  `text` VARCHAR(45) NOT NULL,
  `slika` BLOB NULL,
  `video` BLOB NULL,
  `audio` BLOB NULL,
  `datum` TIMESTAMP NOT NULL,
  PRIMARY KEY (`idzanimljivosti`),
  UNIQUE INDEX `idzanimljivosti_UNIQUE` (`idzanimljivosti` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`festival`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`festival` (
  `idfestival` INT NOT NULL AUTO_INCREMENT,
  `naziv` VARCHAR(45) NOT NULL,
  `opis` VARCHAR(45) NOT NULL,
  `datumpocetak` DATE NOT NULL,
  `datumkraj` DATE NOT NULL,
  `zanimljivosti_idzanimljivosti` INT NOT NULL,
  PRIMARY KEY (`idfestival`),
  UNIQUE INDEX `idfestival_UNIQUE` (`idfestival` ASC) ,
  INDEX `fk_festival_zanimljivosti1_idx` (`zanimljivosti_idzanimljivosti` ASC) ,
  CONSTRAINT `fk_festival_zanimljivosti1`
    FOREIGN KEY (`zanimljivosti_idzanimljivosti`)
    REFERENCES `db_musicmax`.`zanimljivosti` (`idzanimljivosti`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`komentar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`komentar` (
  `idkomentar` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(45) NOT NULL,
  `korisnik_idkorisnik` INT NOT NULL,
  `festival_idfestival` INT NOT NULL,
  `pesma_idpesma` INT NOT NULL,
  PRIMARY KEY (`idkomentar`),
  UNIQUE INDEX `idutisak_UNIQUE` (`idkomentar` ASC) ,
  INDEX `fk_utisak_korisnik1_idx` (`korisnik_idkorisnik` ASC) ,
  INDEX `fk_komentar_festival1_idx` (`festival_idfestival` ASC) ,
  INDEX `fk_komentar_pesma1_idx` (`pesma_idpesma` ASC) ,
  CONSTRAINT `fk_utisak_korisnik1`
    FOREIGN KEY (`korisnik_idkorisnik`)
    REFERENCES `db_musicmax`.`korisnik` (`idkorisnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_komentar_festival1`
    FOREIGN KEY (`festival_idfestival`)
    REFERENCES `db_musicmax`.`festival` (`idfestival`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_komentar_pesma1`
    FOREIGN KEY (`pesma_idpesma`)
    REFERENCES `db_musicmax`.`pesma` (`idpesma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`kategorija`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`kategorija` (
  `idkategorija` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `opis` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idkategorija`),
  UNIQUE INDEX `idkategorija_UNIQUE` (`idkategorija` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`tema`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`tema` (
  `idtema` INT NOT NULL AUTO_INCREMENT,
  `naslov` VARCHAR(45) NOT NULL,
  `datum` DATETIME NOT NULL,
  `kategorija_idkategorija` INT NOT NULL,
  `korisnik_idkorisnik` INT NOT NULL,
  PRIMARY KEY (`idtema`),
  UNIQUE INDEX `idtema_UNIQUE` (`idtema` ASC) ,
  INDEX `fk_tema_kategorija1_idx` (`kategorija_idkategorija` ASC) ,
  INDEX `fk_tema_korisnik1_idx` (`korisnik_idkorisnik` ASC) ,
  CONSTRAINT `fk_tema_kategorija1`
    FOREIGN KEY (`kategorija_idkategorija`)
    REFERENCES `db_musicmax`.`kategorija` (`idkategorija`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tema_korisnik1`
    FOREIGN KEY (`korisnik_idkorisnik`)
    REFERENCES `db_musicmax`.`korisnik` (`idkorisnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`odgovor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`odgovor` (
  `idodgovor` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(45) NOT NULL,
  `datum` TIMESTAMP NOT NULL,
  `korisnik_idkorisnik` INT NOT NULL,
  `tema_idtema` INT NOT NULL,
  PRIMARY KEY (`idodgovor`),
  UNIQUE INDEX `idodgovor_UNIQUE` (`idodgovor` ASC) ,
  INDEX `fk_odgovor_korisnik1_idx` (`korisnik_idkorisnik` ASC) ,
  INDEX `fk_odgovor_tema1_idx` (`tema_idtema` ASC) ,
  CONSTRAINT `fk_odgovor_korisnik1`
    FOREIGN KEY (`korisnik_idkorisnik`)
    REFERENCES `db_musicmax`.`korisnik` (`idkorisnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_odgovor_tema1`
    FOREIGN KEY (`tema_idtema`)
    REFERENCES `db_musicmax`.`tema` (`idtema`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`uloga`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`uloga` (
  `iduloga` INT NOT NULL AUTO_INCREMENT,
  `naziv` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`iduloga`),
  UNIQUE INDEX `iduloga_UNIQUE` (`iduloga` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_musicmax`.`uloga_has_korisnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_musicmax`.`uloga_has_korisnik` (
  `uloga_iduloga` INT NOT NULL,
  `korisnik_idkorisnik` INT NOT NULL,
  PRIMARY KEY (`uloga_iduloga`, `korisnik_idkorisnik`),
  INDEX `fk_uloga_has_korisnik_korisnik1_idx` (`korisnik_idkorisnik` ASC) ,
  INDEX `fk_uloga_has_korisnik_uloga1_idx` (`uloga_iduloga` ASC) ,
  CONSTRAINT `fk_uloga_has_korisnik_uloga1`
    FOREIGN KEY (`uloga_iduloga`)
    REFERENCES `db_musicmax`.`uloga` (`iduloga`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_uloga_has_korisnik_korisnik1`
    FOREIGN KEY (`korisnik_idkorisnik`)
    REFERENCES `db_musicmax`.`korisnik` (`idkorisnik`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

ALTER TABLE `pesma` CHANGE `vremenastanka` `vremenastanka` INT(4) NOT NULL;
ALTER TABLE `korisnik` ADD UNIQUE( `email`);
ALTER TABLE `korisnik` CHANGE `sifra` `sifra` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
