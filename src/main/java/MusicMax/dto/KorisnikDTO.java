package MusicMax.dto;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class KorisnikDTO {

    private int id;
    @NotBlank(message = "Ime je obavezno.")
    private String ime;
    @NotBlank(message = "Prezime je obavezno.")
    private String prezime;
    @NotBlank(message = "Email je obavezan.")
    private String email;
    @NotBlank(message = "Sifra je obavezna.")
    private String sifra;
    private int brojPesama;
    private List<UlogaDTO> ulogas;
    private String token;

    public KorisnikDTO() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    public int getBrojPesama() {
        return brojPesama;
    }

    public void setBrojPesama(int brojPesama) {
        this.brojPesama = brojPesama;
    }

    public List<UlogaDTO> getUlogas() {
        return ulogas;
    }

    public void setUlogas(List<UlogaDTO> ulogas) {
        this.ulogas = ulogas;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
