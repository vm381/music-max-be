package MusicMax.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "pravac")
public class Pravac {
    private Integer idpravac;
    private String naziv;
    private Set<Pesma> pesmas;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpravac")
    public Integer getIdpravac() {
        return idpravac;
    }

    public void setIdpravac(Integer idpravac) {
        this.idpravac = idpravac;
    }

    @Basic
    @Column(name = "naziv")
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pravac pravac = (Pravac) o;
        return Objects.equals(idpravac, pravac.idpravac) &&
                Objects.equals(naziv, pravac.naziv);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idpravac, naziv);
    }

    @OneToMany(mappedBy = "pravac")
    public Set<Pesma> getPesmas() {
        return pesmas;
    }

    public void setPesmas(Set<Pesma> pesmas) {
        this.pesmas = pesmas;
    }
}
