package MusicMax.model;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "pesma")
public class Pesma {
    private Integer idpesma;
    private String naziv;
    private Integer vremenastanka;
    private byte[] audiozapis;
    private byte[] videozapis;
    private Set<Komentar> komentars;
    private Izvodjac izvodjac;
    private Korisnik korisnik;
    private Autor autor;
    private Pravac pravac;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpesma")
    public Integer getIdpesma() {
        return idpesma;
    }

    public void setIdpesma(Integer idpesma) {
        this.idpesma = idpesma;
    }

    @Basic
    @Column(name = "naziv")
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Basic
    @Column(name = "vremenastanka")
    public Integer getVremenastanka() {
        return vremenastanka;
    }

    public void setVremenastanka(Integer vremenastanka) {
        this.vremenastanka = vremenastanka;
    }

    @Basic
    @Column(name = "audiozapis")
    public byte[] getAudiozapis() {
        return audiozapis;
    }

    public void setAudiozapis(byte[] audiozapis) {
        this.audiozapis = audiozapis;
    }

    @Basic
    @Column(name = "videozapis")
    public byte[] getVideozapis() {
        return videozapis;
    }

    public void setVideozapis(byte[] videozapis) {
        this.videozapis = videozapis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pesma pesma = (Pesma) o;
        return Objects.equals(idpesma, pesma.idpesma) &&
                Objects.equals(naziv, pesma.naziv) &&
                Objects.equals(vremenastanka, pesma.vremenastanka) &&
                Arrays.equals(audiozapis, pesma.audiozapis) &&
                Arrays.equals(videozapis, pesma.videozapis);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(idpesma, naziv, vremenastanka);
        result = 31 * result + Arrays.hashCode(audiozapis);
        result = 31 * result + Arrays.hashCode(videozapis);
        return result;
    }

    @OneToMany(mappedBy = "pesma")
    public Set<Komentar> getKomentars() {
        return komentars;
    }

    public void setKomentars(Set<Komentar> komentars) {
        this.komentars = komentars;
    }

    @ManyToOne
    @JoinColumn(name = "izvodjac_idizvodjac", referencedColumnName = "idizvodjac", nullable = false)
    public Izvodjac getIzvodjac() {
        return izvodjac;
    }

    public void setIzvodjac(Izvodjac izvodjac) {
        this.izvodjac = izvodjac;
    }

    @ManyToOne
    @JoinColumn(name = "korisnik_idkorisnik", referencedColumnName = "idkorisnik", nullable = false)
    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @ManyToOne
    @JoinColumn(name = "autor_idautor", referencedColumnName = "idautor", nullable = false)
    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    @ManyToOne
    @JoinColumn(name = "pravac_idpravac", referencedColumnName = "idpravac", nullable = false)
    public Pravac getPravac() {
        return pravac;
    }

    public void setPravac(Pravac pravac) {
        this.pravac = pravac;
    }
}
