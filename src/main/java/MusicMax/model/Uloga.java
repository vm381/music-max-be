package MusicMax.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "uloga")
public class Uloga {
    private Integer iduloga;
    private String naziv;
    private Set<Korisnik> korisniks;

    public Uloga() {
        korisniks = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iduloga")
    public Integer getIduloga() {
        return iduloga;
    }

    public void setIduloga(Integer iduloga) {
        this.iduloga = iduloga;
    }

    @Basic
    @Column(name = "naziv")
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Uloga uloga = (Uloga) o;
        return Objects.equals(iduloga, uloga.iduloga) &&
                Objects.equals(naziv, uloga.naziv);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iduloga, naziv);
    }

    @ManyToMany(mappedBy = "ulogas")
    public Set<Korisnik> getKorisniks() {
        return korisniks;
    }

    public void setKorisniks(Set<Korisnik> korisniks) {
        this.korisniks = korisniks;
    }
}
