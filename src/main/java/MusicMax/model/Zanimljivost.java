package MusicMax.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "zanimljivosti")
public class Zanimljivost {
    private Integer idzanimljivosti;
    private String naslov;
    private String text;
    private byte[] slika;
    private byte[] video;
    private byte[] audio;
    private Timestamp datum;
    private Set<Festival> festivals;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idzanimljivosti")
    public Integer getIdzanimljivosti() {
        return idzanimljivosti;
    }

    public void setIdzanimljivosti(Integer idzanimljivosti) {
        this.idzanimljivosti = idzanimljivosti;
    }

    @Basic
    @Column(name = "naslov")
    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "slika")
    public byte[] getSlika() {
        return slika;
    }

    public void setSlika(byte[] slika) {
        this.slika = slika;
    }

    @Basic
    @Column(name = "video")
    public byte[] getVideo() {
        return video;
    }

    public void setVideo(byte[] video) {
        this.video = video;
    }

    @Basic
    @Column(name = "audio")
    public byte[] getAudio() {
        return audio;
    }

    public void setAudio(byte[] audio) {
        this.audio = audio;
    }

    @Basic
    @Column(name = "datum")
    public Timestamp getDatum() {
        return datum;
    }

    public void setDatum(Timestamp datum) {
        this.datum = datum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zanimljivost that = (Zanimljivost) o;
        return Objects.equals(idzanimljivosti, that.idzanimljivosti) &&
                Objects.equals(naslov, that.naslov) &&
                Objects.equals(text, that.text) &&
                Arrays.equals(slika, that.slika) &&
                Arrays.equals(video, that.video) &&
                Arrays.equals(audio, that.audio) &&
                Objects.equals(datum, that.datum);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(idzanimljivosti, naslov, text, datum);
        result = 31 * result + Arrays.hashCode(slika);
        result = 31 * result + Arrays.hashCode(video);
        result = 31 * result + Arrays.hashCode(audio);
        return result;
    }

    @OneToMany(mappedBy = "zanimljivost")
    public Set<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(Set<Festival> festivals) {
        this.festivals = festivals;
    }
}
