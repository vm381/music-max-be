package MusicMax.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "autor")
public class Autor {
    private Integer idautor;
    private String ime;
    private String prezime;
    private Set<Pesma> pesmas;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idautor")
    public Integer getIdautor() {
        return idautor;
    }

    public void setIdautor(Integer idautor) {
        this.idautor = idautor;
    }

    @Basic
    @Column(name = "ime")
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    @Basic
    @Column(name = "prezime")
    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Autor autor = (Autor) o;
        return Objects.equals(idautor, autor.idautor) &&
                Objects.equals(ime, autor.ime) &&
                Objects.equals(prezime, autor.prezime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idautor, ime, prezime);
    }

    @OneToMany(mappedBy = "autor")
    public Set<Pesma> getPesmas() {
        return pesmas;
    }

    public void setPesmas(Set<Pesma> pesmas) {
        this.pesmas = pesmas;
    }
}
