package MusicMax.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "komentar")
public class Komentar {
    private Integer idkomentar;
    private String text;
    private Korisnik korisnik;
    private Festival festival;
    private Pesma pesma;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idkomentar")
    public Integer getIdkomentar() {
        return idkomentar;
    }

    public void setIdkomentar(Integer idkomentar) {
        this.idkomentar = idkomentar;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Komentar komentar = (Komentar) o;
        return Objects.equals(idkomentar, komentar.idkomentar) &&
                Objects.equals(text, komentar.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idkomentar, text);
    }

    @ManyToOne
    @JoinColumn(name = "korisnik_idkorisnik", referencedColumnName = "idkorisnik", nullable = false)
    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @ManyToOne
    @JoinColumn(name = "festival_idfestival", referencedColumnName = "idfestival", nullable = false)
    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    @ManyToOne
    @JoinColumn(name = "pesma_idpesma", referencedColumnName = "idpesma", nullable = false)
    public Pesma getPesma() {
        return pesma;
    }

    public void setPesma(Pesma pesma) {
        this.pesma = pesma;
    }
}
