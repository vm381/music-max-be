package MusicMax.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "festival")
public class Festival {
    private Integer idfestival;
    private String naziv;
    private String opis;
    private Date datumpocetak;
    private Date datumkraj;
    private Zanimljivost zanimljivost;
    private Set<Komentar> komentars;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idfestival")
    public Integer getIdfestival() {
        return idfestival;
    }

    public void setIdfestival(Integer idfestival) {
        this.idfestival = idfestival;
    }

    @Basic
    @Column(name = "naziv")
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Basic
    @Column(name = "opis")
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Basic
    @Column(name = "datumpocetak")
    public Date getDatumpocetak() {
        return datumpocetak;
    }

    public void setDatumpocetak(Date datumpocetak) {
        this.datumpocetak = datumpocetak;
    }

    @Basic
    @Column(name = "datumkraj")
    public Date getDatumkraj() {
        return datumkraj;
    }

    public void setDatumkraj(Date datumkraj) {
        this.datumkraj = datumkraj;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Festival festival = (Festival) o;
        return Objects.equals(idfestival, festival.idfestival) &&
                Objects.equals(naziv, festival.naziv) &&
                Objects.equals(opis, festival.opis) &&
                Objects.equals(datumpocetak, festival.datumpocetak) &&
                Objects.equals(datumkraj, festival.datumkraj);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idfestival, naziv, opis, datumpocetak, datumkraj);
    }

    @ManyToOne
    @JoinColumn(name = "zanimljivosti_idzanimljivosti", referencedColumnName = "idzanimljivosti", nullable = false)
    public Zanimljivost getZanimljivost() {
        return zanimljivost;
    }

    public void setZanimljivost(Zanimljivost zanimljivost) {
        this.zanimljivost = zanimljivost;
    }

    @OneToMany(mappedBy = "festival")
    public Set<Komentar> getKomentars() {
        return komentars;
    }

    public void setKomentars(Set<Komentar> komentars) {
        this.komentars = komentars;
    }
}
