package MusicMax.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "odgovor")
public class Odgovor {
    private Integer idodgovor;
    private String text;
    private Timestamp datum;
    private Korisnik korisnik;
    private Tema tema;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idodgovor")
    public Integer getIdodgovor() {
        return idodgovor;
    }

    public void setIdodgovor(Integer idodgovor) {
        this.idodgovor = idodgovor;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "datum")
    public Timestamp getDatum() {
        return datum;
    }

    public void setDatum(Timestamp datum) {
        this.datum = datum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Odgovor odgovor = (Odgovor) o;
        return Objects.equals(idodgovor, odgovor.idodgovor) &&
                Objects.equals(text, odgovor.text) &&
                Objects.equals(datum, odgovor.datum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idodgovor, text, datum);
    }

    @ManyToOne
    @JoinColumn(name = "korisnik_idkorisnik", referencedColumnName = "idkorisnik", nullable = false)
    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @ManyToOne
    @JoinColumn(name = "tema_idtema", referencedColumnName = "idtema", nullable = false)
    public Tema getTema() {
        return tema;
    }

    public void setTema(Tema tema) {
        this.tema = tema;
    }
}
