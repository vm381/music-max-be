package MusicMax.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "kategorija")
public class Kategorija {
    private Integer idkategorija;
    private String ime;
    private String opis;
    private Set<Tema> temas;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idkategorija")
    public Integer getIdkategorija() {
        return idkategorija;
    }

    public void setIdkategorija(Integer idkategorija) {
        this.idkategorija = idkategorija;
    }

    @Basic
    @Column(name = "ime")
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    @Basic
    @Column(name = "opis")
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kategorija that = (Kategorija) o;
        return Objects.equals(idkategorija, that.idkategorija) &&
                Objects.equals(ime, that.ime) &&
                Objects.equals(opis, that.opis);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idkategorija, ime, opis);
    }

    @OneToMany(mappedBy = "kategorija")
    public Set<Tema> getTemas() {
        return temas;
    }

    public void setTemas(Set<Tema> temas) {
        this.temas = temas;
    }
}
