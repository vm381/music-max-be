package MusicMax.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "korisnik")
public class Korisnik {
    private Integer idkorisnik;
    private String ime;
    private String prezime;
    private String email;
    private String sifra;
    private Integer brojpesama;
    private Set<Komentar> komentars;
    private Set<Odgovor> odgovors;
    private Set<Pesma> pesmas;
    private Set<Tema> temas;
    private Set<Uloga> ulogas;

    public Korisnik() {
        ulogas = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idkorisnik")
    public Integer getIdkorisnik() {
        return idkorisnik;
    }

    public void setIdkorisnik(Integer idkorisnik) {
        this.idkorisnik = idkorisnik;
    }

    @Basic
    @Column(name = "ime")
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    @Basic
    @Column(name = "prezime")
    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "sifra")
    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    @Basic
    @Column(name = "brojpesama")
    public Integer getBrojpesama() {
        return brojpesama;
    }

    public void setBrojpesama(Integer brojpesama) {
        this.brojpesama = brojpesama;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Korisnik korisnik = (Korisnik) o;
        return Objects.equals(idkorisnik, korisnik.idkorisnik) &&
                Objects.equals(ime, korisnik.ime) &&
                Objects.equals(prezime, korisnik.prezime) &&
                Objects.equals(email, korisnik.email) &&
                Objects.equals(sifra, korisnik.sifra) &&
                Objects.equals(brojpesama, korisnik.brojpesama);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idkorisnik, ime, prezime, email, sifra, brojpesama);
    }

    @OneToMany(mappedBy = "korisnik")
    public Set<Komentar> getKomentars() {
        return komentars;
    }

    public void setKomentars(Set<Komentar> komentars) {
        this.komentars = komentars;
    }

    @OneToMany(mappedBy = "korisnik")
    public Set<Odgovor> getOdgovors() {
        return odgovors;
    }

    public void setOdgovors(Set<Odgovor> odgovors) {
        this.odgovors = odgovors;
    }

    @OneToMany(mappedBy = "korisnik")
    public Set<Pesma> getPesmas() {
        return pesmas;
    }

    public void setPesmas(Set<Pesma> pesmas) {
        this.pesmas = pesmas;
    }

    @OneToMany(mappedBy = "korisnik")
    public Set<Tema> getTemas() {
        return temas;
    }

    public void setTemas(Set<Tema> temas) {
        this.temas = temas;
    }

    @ManyToMany
    @JoinTable(name = "uloga_has_korisnik", catalog = "", schema = "db_musicmax", joinColumns = @JoinColumn(name = "korisnik_idkorisnik", referencedColumnName = "idkorisnik", nullable = false), inverseJoinColumns = @JoinColumn(name = "uloga_iduloga", referencedColumnName = "iduloga", nullable = false))
    public Set<Uloga> getUlogas() {
        return ulogas;
    }

    public void setUlogas(Set<Uloga> ulogas) {
        this.ulogas = ulogas;
    }
}
