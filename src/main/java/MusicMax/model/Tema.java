package MusicMax.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "tema")
public class Tema {
    private Integer idtema;
    private String naslov;
    private Timestamp datum;
    private Set<Odgovor> odgovors;
    private Kategorija kategorija;
    private Korisnik korisnik;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtema")
    public Integer getIdtema() {
        return idtema;
    }

    public void setIdtema(Integer idtema) {
        this.idtema = idtema;
    }

    @Basic
    @Column(name = "naslov")
    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    @Basic
    @Column(name = "datum")
    public Timestamp getDatum() {
        return datum;
    }

    public void setDatum(Timestamp datum) {
        this.datum = datum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tema tema = (Tema) o;
        return Objects.equals(idtema, tema.idtema) &&
                Objects.equals(naslov, tema.naslov) &&
                Objects.equals(datum, tema.datum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idtema, naslov, datum);
    }

    @OneToMany(mappedBy = "tema")
    public Set<Odgovor> getOdgovors() {
        return odgovors;
    }

    public void setOdgovors(Set<Odgovor> odgovors) {
        this.odgovors = odgovors;
    }

    @ManyToOne
    @JoinColumn(name = "kategorija_idkategorija", referencedColumnName = "idkategorija", nullable = false)
    public Kategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(Kategorija kategorija) {
        this.kategorija = kategorija;
    }

    @ManyToOne
    @JoinColumn(name = "korisnik_idkorisnik", referencedColumnName = "idkorisnik", nullable = false)
    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }
}
