package MusicMax.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "izvodjac")
public class Izvodjac {
    private Integer idizvodjac;
    private String ime;
    private String prezime;
    private Set<Pesma> pesmas;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idizvodjac")
    public Integer getIdizvodjac() {
        return idizvodjac;
    }

    public void setIdizvodjac(Integer idizvodjac) {
        this.idizvodjac = idizvodjac;
    }

    @Basic
    @Column(name = "ime")
    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    @Basic
    @Column(name = "prezime")
    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Izvodjac izvodjac = (Izvodjac) o;
        return Objects.equals(idizvodjac, izvodjac.idizvodjac) &&
                Objects.equals(ime, izvodjac.ime) &&
                Objects.equals(prezime, izvodjac.prezime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idizvodjac, ime, prezime);
    }

    @OneToMany(mappedBy = "izvodjac")
    public Set<Pesma> getPesmas() {
        return pesmas;
    }

    public void setPesmas(Set<Pesma> pesmas) {
        this.pesmas = pesmas;
    }
}
