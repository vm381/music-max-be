package MusicMax.controller;

import MusicMax.dto.KorisnikDTO;
import MusicMax.dto.LoginDTO;
import MusicMax.model.Korisnik;
import MusicMax.security.TokenUtils;
import MusicMax.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.ValidationException;

@RestController
@RequestMapping("auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    @Qualifier("userDetailsServiceImpl")
    private UserDetailsService userDetailsService;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private KorisnikService userService;

    @PostMapping("/login")
    public ResponseEntity login(@Valid @RequestBody LoginDTO loginDTO) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginDTO.getEmail(), loginDTO.getSifra());
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getEmail());
            KorisnikDTO userDTO = userService.getKorisnikByEmail(loginDTO.getEmail());
            userDTO.setToken(tokenUtils.generateToken(details));

            /*
            String respBody = "{" +
                    "\"email\": \"" + details.getUsername() + "\"," +
                    "\"role\": \"" + userDTO.getUlogas() + "\"," +
                    "\"token\": \"" + tokenUtils.generateToken(details) + "\"" +
                    "}";
            */

            return new ResponseEntity(userDTO, HttpStatus.OK);
        } catch (ValidationException exp) {
            exp.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/register")
    public ResponseEntity registerUser(@Valid @RequestBody KorisnikDTO user) {
        KorisnikDTO registeredUser = userService.register(user);

        if (registeredUser == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

}
