package MusicMax.controller;

import MusicMax.dto.KorisnikDTO;
import MusicMax.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("korisnik")
public class KorisnikController {

    @Autowired
    private KorisnikService korisnikService;

    @GetMapping
    public ResponseEntity getKorisnik(@RequestParam String email) {
        KorisnikDTO korisnik = korisnikService.getKorisnikByEmail(email);

        return new ResponseEntity(korisnik, HttpStatus.OK);
    }

}
