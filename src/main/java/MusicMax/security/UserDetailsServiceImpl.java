package MusicMax.security;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import MusicMax.model.Korisnik;
import MusicMax.model.Uloga;
import MusicMax.repository.KorisnikRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private KorisnikRepo userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Korisnik user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
		/*List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		for (UserAuthority ua: user.getUserAuthorities()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(ua.getAuthority().getName()));
		}*/

            //Java 1.8 way
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            for (Uloga uloga : user.getUlogas()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(uloga.getNaziv()));
            }

            return new org.springframework.security.core.userdetails.User(
                    user.getEmail(),
                    user.getSifra(),
                    grantedAuthorities);
        }
    }

}