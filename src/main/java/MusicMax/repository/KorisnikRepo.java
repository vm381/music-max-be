package MusicMax.repository;

import MusicMax.model.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KorisnikRepo extends JpaRepository<Korisnik, Integer> {

    Korisnik findByEmail(String email);

}
