package MusicMax.service;

import MusicMax.dto.KorisnikDTO;
import MusicMax.dto.LoginDTO;

public interface KorisnikService {

    KorisnikDTO getKorisnikByEmail(String email);
    KorisnikDTO register(KorisnikDTO korisnikDTO);

}
