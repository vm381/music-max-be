package MusicMax.service;

import MusicMax.dto.KorisnikDTO;
import MusicMax.dto.UlogaDTO;
import MusicMax.model.Korisnik;
import MusicMax.model.Uloga;
import MusicMax.repository.KorisnikRepo;
import MusicMax.repository.UlogaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    private KorisnikRepo korisnikRepo;
    @Autowired
    private UlogaRepo ulogaRepo;

    @Override
    public KorisnikDTO getKorisnikByEmail(String email) {
        Korisnik korisnik = korisnikRepo.findByEmail(email);

        return getKorisnikDTO(korisnik);
    }

    @Override
    public KorisnikDTO register(KorisnikDTO korisnikDTO) {

        Korisnik existingUser = korisnikRepo.findByEmail(korisnikDTO.getEmail());
        if (existingUser != null) {
            return null;
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        Korisnik korisnik = new Korisnik();

        korisnik.setIme(korisnikDTO.getIme());
        korisnik.setPrezime(korisnikDTO.getPrezime());
        korisnik.setEmail(korisnikDTO.getEmail());
        korisnik.setSifra(encoder.encode(korisnikDTO.getSifra()));
        korisnik.setBrojpesama(korisnikDTO.getBrojPesama());

        Uloga uloga = ulogaRepo.findByNaziv("regular");
        uloga.getKorisniks().add(korisnik);

        korisnik.getUlogas().add(uloga);

        Korisnik noviKorisnik = korisnikRepo.save(korisnik);

        if (noviKorisnik == null) {
            return null;
        }

        return getKorisnikDTO(noviKorisnik);
    }

    private KorisnikDTO getKorisnikDTO(Korisnik korisnik) {
        KorisnikDTO korisnikDTO = new KorisnikDTO();

        korisnikDTO.setId(korisnik.getIdkorisnik());
        korisnikDTO.setIme(korisnik.getIme());
        korisnikDTO.setPrezime(korisnik.getPrezime());
        korisnikDTO.setEmail(korisnik.getEmail());
        korisnikDTO.setBrojPesama(korisnik.getBrojpesama());

        List<UlogaDTO> ulogas = new ArrayList<>();

        for (Uloga uloga : korisnik.getUlogas()) {
            UlogaDTO ulogaDTO = new UlogaDTO();

            ulogaDTO.setId(uloga.getIduloga());
            ulogaDTO.setNaziv(uloga.getNaziv());

            ulogas.add(ulogaDTO);
        }

        korisnikDTO.setUlogas(ulogas);

        return  korisnikDTO;
    }

}
